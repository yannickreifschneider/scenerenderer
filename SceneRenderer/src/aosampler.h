/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#ifndef AOSAMPLER_H_INCLUDED
#define AOSAMPLER_H_INCLUDED

#include "geometry.h"

class vec3;

class AOSampler
{
public:
	AOSampler();
	/// constructor that initializes the sampler, just calls init
	AOSampler(vec3 const & normal, vec3 const & color);
	/// initializes the sampler
	void init(vec3 const & normal, vec3 const & color);
	/// returns an importance sampled random direction and the associated weight
	void sample(vec3 & sampledDirection, vec3 & sampleWeight) const;
private:
	/// orthonormal basis
	vec3 m_normal;
	vec3 m_tangent;
	vec3 m_bitangent;
	/// diffuse color
	vec3 m_color;
};


#endif //AOSAMPLER_H_INCLUDED