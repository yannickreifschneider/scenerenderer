/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#ifndef GEOMETRY_H_INCLUDED
#define GEOMETRY_H_INCLUDED

#include "vec.h"
#include <cfloat> // needed for FLT_MAX
#include <algorithm> // needed for std min and max

#define VAL_PI       3.14159265358979323846f
#define VAL_INV_PI     0.318309886183790671538f

class Ray
{
public:
	Ray() : tMin(0.001f), tMax(FLT_MAX){}
	vec3 origin;
	vec3 direction;
	float tMin, tMax;
};

struct Intersection
{
	vec3 position;
	vec3 normal;
	vec3 color;
};

struct PositionNormal
{
	vec3 position;
	vec3 normal;
};

class AABB
{
public:
	/// the AABB's minimum corner
	vec3 minimum;
	/// the AABB's maximum corner
	vec3 maximum;

	///  setter function
	void set(float minX, float maxX, float minY, float maxY, float minZ, float maxZ)
	{
		minimum.x = minX; maximum.x = maxX;
		minimum.y = minY; maximum.y = maxY;
		minimum.z = minZ; maximum.z = maxZ;
	}

	/// setter function
	void set(vec3 const & v)
	{
		minimum = v;
		maximum = v;
	}

	/// set minimum to maximum non-infinity float value
	/// set maximum to minimum negativ non-infinity value
	void invalidate()
	{
		minimum = FLT_MAX;
		maximum = -FLT_MAX;
	}

	/// returns true, if any coordinate of the maximum is smaller than the minimum
	/// otherwise returns false
	bool isInvalid() const
	{
		return maximum.x < minimum.x || maximum.y < minimum.y || maximum.z < minimum.z;
	}

	/// degenerates the AABB to the given point
	void operator=(vec3 const & v)
	{
		set(v);
	}

	/// expand the AABB with a point
	/// the AABB is unchanged, if the point lies inside the box
	void merge(vec3 const & v)
	{
		// set maximum to maximum of maxima
		// set minimum to minima of minima
		if(minimum.x > v.x)
			minimum.x = v.x;
		if(maximum.x < v.x)
			maximum.x = v.x;
		if(minimum.y > v.y)
			minimum.y = v.y;
		if(maximum.y < v.y)
			maximum.y = v.y;
		if(minimum.z > v.z)
			minimum.z = v.z;
		if(maximum.z < v.z)
			maximum.z = v.z;
	}

	/// expand the AABB with a point
	/// the AABB is unchanged, if the point lies inside the box
	void operator+=(vec3 const & v)
	{
		merge(v);
	}

	/// merge the AABB with another bounding box
	void merge(AABB const & bb)
	{
		// set maximum to maximum of maxima
		// set minimum to minima of minima
		if(minimum.x > bb.minimum.x)
			minimum.x = bb.minimum.x;
		if(maximum.x < bb.maximum.x)
			maximum.x = bb.maximum.x;
		if(minimum.y > bb.minimum.y)
			minimum.y = bb.minimum.y;
		if(maximum.y < bb.maximum.y)
			maximum.y = bb.maximum.y;
		if(minimum.z > bb.minimum.z)
			minimum.z = bb.minimum.z;
		if(maximum.z < bb.maximum.z)
			maximum.z = bb.maximum.z;
	}

	void operator+=(AABB const & bb)
	{
		merge(bb);
	}

	/// intersects the AABB with another AABB and returns the resulting intersection box
	/// the returned AABB is invalid, if the intersection is empty
	AABB intersect(AABB const & bb) const
	{
		AABB result;

		// compute minima of maxima and maxima of minima
		result.minimum.x = minimum.x > bb.minimum.x ? minimum.x : bb.minimum.x;
		result.minimum.y = minimum.y > bb.minimum.y ? minimum.y : bb.minimum.y;
		result.minimum.z = minimum.z > bb.minimum.z ? minimum.z : bb.minimum.z;

		result.maximum.x = maximum.x < bb.maximum.x ? maximum.x : bb.maximum.x;
		result.maximum.y = maximum.y < bb.maximum.y ? maximum.y : bb.maximum.y;
		result.maximum.z = maximum.z < bb.maximum.z ? maximum.z : bb.maximum.z;

		return result;
	}

	/// returns the surface area of the AABB
	float getSurfaceArea() const
	{
		vec3 extents = maximum - minimum;
		return 2.0f * (extents.x * extents.y + extents.x * extents.z + extents.y * extents.z);
	}
};

inline bool intersectsRayBB(vec3 const & rayOrigin, vec3 const & invRayDir, AABB const & bb, float minT, float maxT)
{
	//check aligned slabs
	vec3 minSlabsT = (bb.minimum - rayOrigin) * invRayDir;
	vec3 maxSlabsT = (bb.maximum - rayOrigin) * invRayDir;

	vec3 sortedMin = vec3::min(minSlabsT, maxSlabsT);
	vec3 sortedMax = vec3::max(minSlabsT, maxSlabsT);

	float closestT = std::max(minT, sortedMin.maxComponent());
	float farthestT = std::min(maxT, sortedMax.minComponent());

	if(closestT > farthestT)
		return false;

	return true;
}

inline bool intersectRayBB(vec3 const & rayOrigin, vec3 const & invRayDir, AABB const & bb, float & closestT, float & farthestT)
{
	//check aligned slabs
	vec3 minSlabsT = (bb.minimum - rayOrigin) * invRayDir;
	vec3 maxSlabsT = (bb.maximum - rayOrigin) * invRayDir;

	vec3 sortedMin = vec3::min(minSlabsT, maxSlabsT);
	vec3 sortedMax = vec3::max(minSlabsT, maxSlabsT);

	closestT = std::max(closestT, sortedMin.maxComponent());
	farthestT = std::min(farthestT, sortedMax.minComponent());

	if(closestT > farthestT)
		return false;

	return true;
}

inline float intersectRayTriangleT(Ray const & ray, vec3 const & p, vec3 const & edge1, vec3 const & edge2)
{
	vec3 tvec = ray.origin - p;
	vec3 pvec = vec3::cross(ray.direction, edge2);
	float det = 1.0f / edge1.dot(pvec);

	float u = tvec.dot(pvec) * det;

	if(u < 0.0f || u > 1.0f)
		return -1.0f;

	pvec = vec3::cross(tvec, edge1);

	float v = ray.direction.dot(pvec) * det;

	if(v < 0.0f || (u + v) > 1.0f)
		return -1.0f;

	return edge2.dot(pvec) * det;
}

inline void intersectRayTriangleUV(Ray const & ray, vec3 const & p, vec3 const & edge1, vec3 const & edge2, float & u, float & v)
{
	vec3 tvec = ray.origin - p;
	vec3 pvec = vec3::cross(ray.direction, edge2);
	float det = 1.0f / edge1.dot(pvec);

	u = tvec.dot(pvec) * det;

	pvec = vec3::cross(tvec, edge1);

	v = ray.direction.dot(pvec) * det;
}

#endif // GEOMETRY_H_INCLUDED