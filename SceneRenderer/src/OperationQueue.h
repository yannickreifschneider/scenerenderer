/*
 * OperationQueue.h
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#ifndef OPERATIONQUEUE_H_
#define OPERATIONQUEUE_H_

#include <vector>
#include <queue>

#include "Mutex.h"
#include "Condition.h"

class Operation;
class Thread;

class OperationQueue {
	friend class Thread;
public:
	OperationQueue(unsigned int);
	~OperationQueue();

	void addOperation(Operation *);
	void waitUntilAllOperationsAreFinished();

private:
	unsigned int m_num_threads;
	std::vector<Thread *> m_threads;
	std::queue<Operation *> m_operations;
	Mutex m_queue_mutex;
	Condition m_queue_condition;
	Mutex m_empty_queue_mutex;
	Condition m_empty_queue_condtion;
};

#endif /* OPERATIONQUEUE_H_ */
