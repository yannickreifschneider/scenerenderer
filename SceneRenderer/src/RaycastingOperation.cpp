/*
 * RaycastingOperation.cpp
 *
 *  Created on: 15.01.2012
 *      Author: yannick
 */

#include "RaycastingOperation.h"
#include "atomics.h"
#include "camera.h"
#include "aosampler.h"
#include "geometry.h"

void RaycastingOperation::run()
{
	float const aoCheckDistance = 800.0f;

	RaycastingOperationArgument *args = (RaycastingOperationArgument *)m_arg;

	for(unsigned int ps = 0; ps < args->num_color_samples; ++ps)
	{
		// get camera ray for the pixel
		Ray ray = args->camera->createRayForPixel(args->x, args->y, true);
		Intersection intersection;

		vec3 color;
		// check for scene intersection
		if( args->scene->findIntersection(ray, intersection) )
		{
			// found intersection, apply AO shading
			color = vec3(0.0f,0.0f,0.0f);
			// initialize sampler
			AOSampler sampler(intersection.normal, intersection.color);
			// init test ray
			Ray testRay;
			testRay.origin = intersection.position;
			testRay.tMax = aoCheckDistance;
			// generate samples
			for(unsigned int s = 0; s < args->num_ao_samples; ++s)
			{
				vec3 contribution;
				sampler.sample(testRay.direction, contribution);
				if(!args->scene->intersects(testRay))
					color += contribution; // add conribution if unoccluded
			}
			color = color / float(args->num_ao_samples);
		} else {
			// set to a default color
			color = vec3(0.9f,0.9f,1.0f);
		}
		color = color / float(args->num_ao_samples);
		// store value in image
		AtomicAddFloat(&args->data[0], color.x);
		AtomicAddFloat(&args->data[1], color.y);
		AtomicAddFloat(&args->data[2], color.z);
/*
		args->data[0] = color.x;
		args->data[1] = color.y;
		args->data[2] = color.z;
		*/
	}
}

RaycastingOperation::~RaycastingOperation()
{
	free(m_arg);
}

