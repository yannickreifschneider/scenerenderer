/*
 * Operation.h
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#ifndef OPERATION_H_
#define OPERATION_H_

class Operation {
public:
	Operation(void *arg) :m_arg(arg) { }
	virtual ~Operation() { }
	virtual void run() = 0;

protected:
	void *m_arg;
};

#endif /* OPERATION_H_ */
