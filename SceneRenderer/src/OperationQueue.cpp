/*
 * OperationQueue.cpp
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#include "OperationQueue.h"
#include "Thread.h"


OperationQueue::OperationQueue(unsigned int num_threads)
: m_num_threads(num_threads)
{
	m_threads.reserve(num_threads);
	for (int i = 0; i < num_threads; i++) {
		Thread *t = new Thread(i, this);
		m_threads.push_back(t);
	}
}

OperationQueue::~OperationQueue()
{
}

void OperationQueue::waitUntilAllOperationsAreFinished()
{
	m_empty_queue_mutex.lock();
	m_empty_queue_condtion.wait(m_empty_queue_mutex);
	m_empty_queue_mutex.unlock();
	std::cout << "received empty queue signal" << std::endl;

	for (int i = 0; i < m_num_threads; i++) {
		m_threads.at(i)->setFinished(true);
	}
	m_queue_mutex.lock();
	m_queue_condition.signal();
	m_queue_mutex.unlock();
	for (int i = 0; i < m_num_threads; i++) {
		std::cout << "waiting for thread " << i << " to finish" << std::endl;
		m_threads.at(i)->join();
	}
	return;
}

void OperationQueue::addOperation(Operation *operation)
{
	m_queue_mutex.lock();
	m_operations.push(operation);
	m_queue_condition.signal();
	m_queue_mutex.unlock();
}

