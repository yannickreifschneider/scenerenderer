/*
 * Mutex.h
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#ifndef MUTEX_H_
#define MUTEX_H_

#include <pthread.h>

class Mutex {
	friend class Condition;
public:
	Mutex();
	~Mutex();

	int lock();
	int unlock();
	int trylock();

protected:
	pthread_mutex_t m_mutex;
};

#endif /* MUTEX_H_ */
