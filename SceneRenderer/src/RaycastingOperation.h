/*
 * RaycastingOperation.h
 *
 *  Created on: 15.01.2012
 *      Author: yannick
 */

#ifndef RAYCASTINGOPERATION_H_
#define RAYCASTINGOPERATION_H_

#include "CScene.h"
#include "camera.h"

typedef struct {
	CScene *scene;
	CCamera *camera;
	int x;
	int y;
	unsigned int num_color_samples;
	unsigned int num_ao_samples;
	float *data;
} RaycastingOperationArgument;

#include "Operation.h"

class RaycastingOperation : public Operation {
public:
	RaycastingOperation(void *arg) : Operation(arg) { };
	virtual ~RaycastingOperation();
	void run();
};

#endif /* RAYCASTINGOPERATION_H_ */
