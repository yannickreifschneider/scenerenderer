/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

// base include
#include "CBVHBuilder.h"

#include "CScene.h"

#include <iostream>
#include <stack>
#include <string.h>

/// Traversal weight for SAH
const float CBVHBuilder::c_traversal = 1.0f;

/// Intersection weight for SAH
const float CBVHBuilder::c_intersection = 0.5f;

//-----------------------------------------------------------------------------
// method        : CBVHBuilder::CBVHBuilder()
// params        : 
// return values : 	
// description   : contructor
//-----------------------------------------------------------------------------
CBVHBuilder::CBVHBuilder() {};

//-----------------------------------------------------------------------------
// method        : CBVHBuilder::~CBVHBuilder()
// params        : 
// return values : 	
// description   : contructor
//-----------------------------------------------------------------------------
CBVHBuilder::~CBVHBuilder() {
}

//-----------------------------------------------------------------------------
// method        : CBVHBuilder::buildBVH()
// params        : 
// return values : 	
// description   : Builds a BVH for the given geometry. Modifies the content of geometry and materials
//-----------------------------------------------------------------------------
bool CBVHBuilder::buildBVH(std::vector<BVHNode> & bvh,
						 std::vector<PositionNormal> & geometry,
						 std::vector<unsigned int> & materials)
{
	std::cout << "Building SAH BVH" << std::endl;

	//-------------------------------------------------------------------------------
	//allocate memory
	//-------------------------------------------------------------------------------

	// get the number of triangles for the scene
	size_t numTriangles = materials.size();
	size_t numVertices	= geometry.size();

	//-------------------------------------------------------------------------------
	// compute triangle bounding boxes
	//-------------------------------------------------------------------------------

	// compute primitives and scene bounding box
	std::list<PrimitiveReference> primitiveReferences;
	AABB sceneBB;
	buildPrimitiveAABBs(geometry, primitiveReferences, sceneBB);

	// arrays for the final output mesh
	std::vector<PositionNormal> resortedGeometry;
	std::vector<unsigned int> resortedMaterials;
	resortedGeometry.reserve(numVertices);
	resortedMaterials.reserve(numTriangles);


	//-------------------------------------------------------------------------------
	// construct bvh with SAH
	//-------------------------------------------------------------------------------
	std::vector<BVHNode> finalBVH;
	buildSAHBVH(finalBVH, sceneBB, primitiveReferences, geometry, materials, resortedGeometry, resortedMaterials);

	
	//-------------------------------------------------------------------------------
	// fetch sorted geometry and bvhs
	//-------------------------------------------------------------------------------
	// copy final bvh and geometry
	bvh.resize(finalBVH.size());
	memcpy(&bvh[0], &finalBVH[0], finalBVH.size() * sizeof(BVHNode));

	memcpy(&geometry[0], &resortedGeometry[0], resortedGeometry.size() * sizeof(PositionNormal));
	memcpy(&materials[0], &resortedMaterials[0], resortedMaterials.size() * sizeof(unsigned int));


	std::cout << "Finished building BVH" << std::endl;
	std::cout << "Number of BVH nodes: " << bvh.size() << std::endl;

	return true;
}

//-----------------------------------------------------------------------------
// method        : CBVHBuilder::buildPrimitiveAABBs()
// params        : 
// return values : 	
// description   : contructor
//-----------------------------------------------------------------------------
void CBVHBuilder::buildPrimitiveAABBs(GeometryVector const & geometry, std::list<PrimitiveReference> & primitiveReferences, AABB & sceneBB)
{
	// get the number of triangles for the scene
	size_t numTriangles = geometry.size() / 3;

	sceneBB.invalidate();

	// iterate over triangles
	for(unsigned int triangleIdx = 0; triangleIdx < numTriangles; triangleIdx++)
	{
		unsigned int vertexIdx = triangleIdx * 3;

		// compute triangle AABB
		PrimitiveReference pr;

		pr.bb = geometry[vertexIdx].position;
		pr.bb += geometry[vertexIdx + 1].position;
		pr.bb += geometry[vertexIdx + 2].position;
		pr.id = triangleIdx;
		pr.dummy = 0;

		// write the bounding box
		primitiveReferences.push_back(pr);

		// update scene bounding box
		sceneBB += pr.bb;
	} // for

	std::cout << "Scene bounding box is: [ ";
	std::cout << sceneBB.minimum.x << ' ' << sceneBB.minimum.y << ' ' << sceneBB.minimum.z;
	std::cout << " ] [ ";
	std::cout << sceneBB.maximum.x << ' ' << sceneBB.maximum.y << ' ' << sceneBB.maximum.z;
	std::cout << " ]" << std::endl;
} // END: buildAABB


//////////////
// SAH construction stuff

/// Partitions a list of triangles
void CBVHBuilder::partition(NodeSplit const & split,
							BVHBuildNode & splitNode,
							std::list<PrimitiveReference> & left,
							std::list<PrimitiveReference> & right)
{

	std::list<PrimitiveReference>::iterator i = splitNode.primitives.begin();

	for(; i != splitNode.primitives.end(); ++i)
	{
		PrimitiveReference const & primitive = *i;
		if((primitive.bb.minimum[split.axis] + primitive.bb.maximum[split.axis]) * 0.5f < split.separator) {
			left.push_back(primitive);
		} else {
			right.push_back(primitive);
		}
	}
	splitNode.primitives.clear();
} // END: partition

	
/// determines an optimal split for a node via SAH
void CBVHBuilder::binning(BVHBuildNode const & splitNode, GeometryVector const & geometry, NodeSplit & optimalSplit)
{
	NodeSplit objectSplits[3][NUM_BINS];

	for(unsigned int binIdx = 0; binIdx < NUM_BINS; ++binIdx)
	{
		// init split information
		vec3 separators = (splitNode.bb.maximum - splitNode.bb.minimum) / NUM_BINS * (binIdx + 0.5f) + splitNode.bb.minimum;
		for(unsigned int axis = 0; axis < 3; ++axis)
		{
			objectSplits[axis][binIdx].lBB.invalidate();
			objectSplits[axis][binIdx].rBB.invalidate();
			objectSplits[axis][binIdx].lCount = 0;
			objectSplits[axis][binIdx].rCount = 0;
			objectSplits[axis][binIdx].axis = axis;
			objectSplits[axis][binIdx].separator = separators[axis];
		}
	}


	// iterate over node triangle boxes
	for( std::list<PrimitiveReference>::const_iterator i = splitNode.primitives.begin(); i != splitNode.primitives.end(); ++i)
	{
		PrimitiveReference const & pr = *i;

		// compute center of aabb
		vec3 center = 0.5f * (pr.bb.minimum + pr.bb.maximum);

		//put primitive into bins
		for(unsigned int binIdx = 0; binIdx < NUM_BINS; ++binIdx)
		{
			for(unsigned int axis = 0; axis < 3; ++axis)
			{
				objectSplits[axis][binIdx].objectSplitPut(pr.bb, center[axis]);
			}
		}
	}

	// done binning. Now find minimum SAH cost split
	float minimumCost = FLT_MAX;
	unsigned int optimalAxis = 0;
	unsigned int optimalBin = 0;

	float parentArea = splitNode.bb.getSurfaceArea();

	for(unsigned int binIdx = 0; binIdx < NUM_BINS; ++binIdx)
	{
		for(unsigned int axis = 0; axis < 3; ++axis)
		{
			float sah;
			sah = objectSplits[axis][binIdx].evaluateSAH(c_traversal, c_intersection, parentArea);
			if(sah < minimumCost)
			{
				minimumCost = sah;
				optimalAxis = axis;
				optimalBin = binIdx;
			}
		}
	}

	optimalSplit = objectSplits[optimalAxis][optimalBin];
} // END: binning


void CBVHBuilder::createLeaf(BVHBuildNode const & buildNode,
						BVHNode & node,
						GeometryVector const & geometry, MaterialVector const & materials,
						GeometryVector & finalGeometry, MaterialVector & finalMaterials)
{
	node.bb = buildNode.bb;

	node.val1 = finalMaterials.size();
	node.val2 = buildNode.primitives.size();

	// dump triangles
	std::list<PrimitiveReference>::const_iterator i;

	for(i = buildNode.primitives.begin(); i != buildNode.primitives.end(); ++i)
	{
		unsigned int triangleIdx = (*i).id;
		unsigned int vertexIdx = triangleIdx * 3;

		//write material
		finalMaterials.push_back(materials[triangleIdx]);

		PositionNormal p[3];
		// read input vertices
		p[0] = geometry[vertexIdx];
		p[1] = geometry[vertexIdx + 1];
		p[2] = geometry[vertexIdx + 2];

		// write first point to correct position
		finalGeometry.push_back(p[0]);

		//the next two points are converted to edges
		p[1].position -= p[0].position;
		finalGeometry.push_back(p[1]);
		p[2].position -= p[0].position;
		finalGeometry.push_back(p[2]);
	}
}

void CBVHBuilder::createInnerNode(BVHBuildNode const & buildNode,
								  BVHNode & node,
								  unsigned int childrenPos)
{
	node.bb = buildNode.bb;
	node.val1 = childrenPos;
	node.val2 = BVHNode::INNER_NODE;
}

#define BUILDSAH_MAX_TRIANGLES_PER_LEAF 4
#define INVALID_NODE_INDEX 0xffffffff

	
/// Constructs a BVH with SAH.
/// Input is a root node, that contains all scene triangles.
void CBVHBuilder::buildSAHBVH(std::vector<BVHNode> & bvh,
						AABB const & sceneBB,
						std::list<PrimitiveReference> & inputPrimitives,
						GeometryVector & geometry, MaterialVector & materials,
						GeometryVector & finalGeometry, MaterialVector & finalMaterials)
{
	unsigned int numTris = inputPrimitives.size();
	unsigned int maxNumberOfSAHBVHNodes = numTris * 2 - 1; // theoretical upper bound for the degenerated tree

	bvh.clear();
	bvh.reserve(maxNumberOfSAHBVHNodes);

	std::vector<BVHBuildNode> bvhBuildnodes;
	bvhBuildnodes.reserve(maxNumberOfSAHBVHNodes);

	// init root node
	bvh.push_back(BVHNode());
	bvhBuildnodes.push_back(BVHBuildNode());
	bvhBuildnodes[0].bb = sceneBB;
	bvhBuildnodes[0].primitives.swap(inputPrimitives);

	typedef std::pair<unsigned int, unsigned int> SplitTask;
	std::stack<SplitTask> nodeStack;
	nodeStack.push(std::make_pair(0u,0u));

	// optimal split data
	NodeSplit optimalSplit;

	while(!nodeStack.empty())
	{ // if there are still nodes to split, than continue

		// fetch id ofa node to be split
		SplitTask splitTask = nodeStack.top();
		nodeStack.pop();
		unsigned int splitNodeId = splitTask.first;
		unsigned int nodeDepth = splitTask.second;

		BVHBuildNode & currentNode = bvhBuildnodes[splitNodeId];

		unsigned int numNodeTriangles = currentNode.primitives.size();

		// if the number of triangles is small enough, make the node a leaf
		if( numNodeTriangles <= BUILDSAH_MAX_TRIANGLES_PER_LEAF || nodeDepth >= 24)
		{
			// Create a leaf
			createLeaf(currentNode, bvh[splitNodeId], geometry, materials, finalGeometry, finalMaterials);
		} else {
			// find the optimal splitting plane for this node via binning
			NodeSplit optimalSplit;
			binning(currentNode, geometry, optimalSplit);
			// check if optimal split produces two non empty nodes
			if(optimalSplit.lCount != 0 && optimalSplit.rCount != 0)
			{
				// have to partition the primitives with respect to the optimal split
				std::list<PrimitiveReference> leftPrimitives, rightPrimitives;
				partition(optimalSplit, currentNode, leftPrimitives, rightPrimitives);
		
				// get a position for the new nodes
				unsigned int newNodesBase = bvh.size();

				createInnerNode(currentNode, bvh[splitNodeId], newNodesBase);										

				// construct and store child nodes
				bvhBuildnodes.push_back(BVHBuildNode());
				bvhBuildnodes.push_back(BVHBuildNode());
				bvhBuildnodes[newNodesBase].bb = optimalSplit.lBB;
				bvhBuildnodes[newNodesBase].primitives.swap(leftPrimitives);
				bvhBuildnodes[newNodesBase+1].bb = optimalSplit.rBB;
				bvhBuildnodes[newNodesBase+1].primitives.swap(rightPrimitives);
				bvh.push_back(BVHNode());
				bvh.push_back(BVHNode());
								
				
				// construct split tasks and push on stack
				SplitTask newSplitTask;
				newSplitTask = std::make_pair(newNodesBase+1, nodeDepth + 1);
				nodeStack.push(newSplitTask);
				newSplitTask = std::make_pair(newNodesBase, nodeDepth + 1);
				nodeStack.push(newSplitTask);
			} else {
				// Create a leaf
				createLeaf(currentNode, bvh[splitNodeId], geometry, materials, finalGeometry, finalMaterials);
			}
		}
	} // END: stack loop
	return;
} // END: buildSAHBVH
