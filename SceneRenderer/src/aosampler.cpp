/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#include "aosampler.h"

#include <cmath>
#include <algorithm>
#include <cstdlib>

AOSampler::AOSampler() {}

AOSampler::AOSampler(vec3 const & normal, vec3 const & color)
{
	// just call the init function
	init(normal, color);
}

void AOSampler::init(vec3 const & normal, vec3 const & color)
{
	// init color
	m_color = color;
	// construct orthonormal basis
	m_normal = normal;
	if(abs(normal.z) < 0.5f) // 0.5f is set arbitrarily, just must be smaller than 1.0f
	{
		m_tangent.x =  normal.y;
		m_tangent.y = -normal.x;
		m_tangent.z = 0.0f;
	} else {
		m_tangent.x =  0;
		m_tangent.y = -normal.z;
		m_tangent.z = normal.y;
	}
	m_tangent = m_tangent.normalize();
	m_bitangent = vec3::cross(m_tangent,normal); // in a perfect world this vector is normalized by construction
}

void AOSampler::sample(vec3 & sampledDirection, vec3 & sampleWeight) const
{
	// want to solve following integral
	// I = integral_omega c / PI * cos(omega_i) d_omega_i
	// with monte carlo integration.
	// can generate samples with nearly optimal pdf cos(omega_i) / PI for efficient importance sampling.

	// generate cosine weighted sample with the uniform sampled disk method
	// first generate uniform disk sample
	float r = sqrt(float(rand()) / RAND_MAX);
	float theta = 2.0f * VAL_PI * (float(rand()) / RAND_MAX);
	float localX = r * cos(theta);
	float localZ = r * sin(theta);
	// project on hemisphere
	float localY = sqrt(std::max(0.0f,1.0f - localX * localX - localZ * localZ));
	sampledDirection = m_tangent * localX + m_normal * localY + m_bitangent * localZ;

	// set sample weight
	sampleWeight = m_color;
}