/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#ifndef VEC_H_INCLUDED
#define VEC_H_INCLUDED

///A 2 component vector
class vec2
{
public:
	float x,y;

	vec2(float a, float b);
	vec2();
};

///A 3 component vector
class vec3
{
public:
	float x,y,z;

	vec3();
	vec3(float a, float b, float c);

	/// assignment operator that assigns a single scalar value to all components
	void operator=(float v);

	/// unsafe element access 
	float operator[](unsigned int i) const
	{
		return (&x)[i];
	}

	/// length of the vector
	float length() const;

	///Returns a normalized version of the vector
	vec3 normalize() const;

	/// componentwise summation
	vec3 add(const vec3& a) const;

	/// componentwise subtraction
	vec3 subtract(const vec3& a) const;

	///compute the dot product which is cos(alpha) * this.Length * a.Length
	///where alpha is the (smaller) angle between the vectors
	float dot(const vec3& a) const;

	float minComponent() const;
	float maxComponent() const;

	///computes a vector which is orthogonal to both of the input vectors
	static vec3 cross(const vec3& a, const vec3& b);
	static vec3 min(const vec3& a, const vec3& b);
	static vec3 max(const vec3& a, const vec3& b);

	/// add a vector to this vector
	void operator+=( vec3 const & v );
	/// subtract a vector from this vector
	void operator-=( vec3 const & v );
};

/// componentwise addition
const vec3 operator+(const vec3& lhs, const vec3& rhs);
/// componentwise subtraction
const vec3 operator-(const vec3& lhs, const vec3& rhs);
const vec3 operator-(const vec3& lhs, const float& rhs);
/// multiplication with a scalar
const vec3 operator*(const float& lhs, const vec3& rhs);
const vec3 operator*(const vec3& lhs, const float& rhs);
/// componentwise multiplication of two vectors
const vec3 operator*(const vec3& lhs, const vec3& rhs);
/// division by a scalar
const vec3 operator/(const vec3& lhs, const float& rhs);
/// divide scalar by a vector
const vec3 operator/(const float& lhs, const vec3& rhs);

#endif // VEC_H_INCLUDED