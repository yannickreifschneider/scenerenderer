/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */


#ifndef BVHBUILDER_H_INCLUDED
#define BVHBUILDER_H_INCLUDED

#include "geometry.h"

#include <list>
#include <vector>


//-----------------------------------------------------------------------------
// class forward
//-----------------------------------------------------------------------------

struct PrimitiveReference
{
	AABB bb;
	unsigned int id;
	unsigned int dummy;
};

struct BVHBuildNode
{
	AABB bb;
	std::list<PrimitiveReference> primitives;
};

struct BVHNode
{
	enum {
		INNER_NODE = 0xffffffffu
	};
	// the nodes aabb
	AABB bb;
	// leaf: start index in triangle list
	// inner node: index to children
	unsigned int val1;
	// leaf: end index in triangle list
	// inner node: set to INNER_NODE
	unsigned int val2;

	/// checks if the node is a leaf
	bool isLeaf() const { return val2 != INNER_NODE;}
};


/// Collects all data for an object split
struct NodeSplit
{
	AABB lBB, rBB;
	unsigned int lCount, rCount;
	unsigned int axis;
	float separator;

	void putLeft(AABB const & aabb)
	{
		++lCount;
		lBB.merge(aabb);
	}

	void putRight(AABB const & aabb)
	{
		++rCount;
		rBB.merge(aabb);
	}

	void objectSplitPut(AABB const & aabb, float v)
	{
		if(v < separator)
			putLeft(aabb);
		else
			putRight(aabb);
	}

	float evaluateSAH( float c_traversal, float c_intersection, float parentArea)
	{
		// left box area
		float lArea;
		if(lCount)
		{
			lArea = lBB.getSurfaceArea();
		} else {
			lArea = 0.0f;
		}
		
		// right box area
		float rArea;
		if(rCount)
		{
			rArea = rBB.getSurfaceArea();
		} else {
			rArea = 0.0f;
		}

		// actually calculating sah cost
		return c_traversal + c_intersection * (lCount * lArea + rCount * rArea) / parentArea;
	}

	float overlapSurfaceArea() const
	{
		AABB overlap = lBB.intersect(rBB);

		if(overlap.isInvalid())
			return 0.0f;

		return overlap.getSurfaceArea();
	}
};

class CBVHBuilder
{
	public:
		typedef std::vector<PositionNormal> GeometryVector;
		typedef std::vector<unsigned int> MaterialVector;

		/// constructor
		CBVHBuilder();

		/// destructor
		~CBVHBuilder();


		/// Runs the bvh build process
		///  @return true if success
		bool buildBVH(std::vector<BVHNode> & bvh,
						 GeometryVector & geometry,
						 MaterialVector & materials);

	private:

		enum {
			NUM_BINS = 32u
		};

		/// Partitions a list of triangles
		void partition( NodeSplit const & split,
						BVHBuildNode & splitNode,
						std::list<PrimitiveReference> & left,
						std::list<PrimitiveReference> & right);

		/// determines an optimal split for a node via SAH
		void binning(BVHBuildNode const & splitNode,
					GeometryVector const & geometry,
					NodeSplit & optimalSplit);

		//
		void createLeaf(BVHBuildNode const & buildNode,
						BVHNode & node,
						GeometryVector const & geometry, MaterialVector const & materials,
						GeometryVector & finalGeometry, MaterialVector & finalMaterials);

		void createInnerNode(BVHBuildNode const & buildNode,
							BVHNode & node,
							unsigned int childrenPos);

		/// Construct AABBs for all triangles and determine scene bounding box
		void buildPrimitiveAABBs(GeometryVector const & geometry, std::list<PrimitiveReference> & primitiveReferences, AABB& sceneBB);

		void buildSAHBVH(std::vector<BVHNode> & bvh,
						AABB const & sceneBB,
						std::list<PrimitiveReference> & inputPrimitives,
						GeometryVector & geometry, MaterialVector & materials,
						GeometryVector & finalGeometry, MaterialVector & finalMaterials);

		/// Traversal weight for SAH
		static const float c_traversal;

		/// Intersection weight for SAH
		static const float c_intersection;
};

#endif // BVHBUILDER_H_INCLUDED