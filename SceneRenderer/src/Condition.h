/*
 * Condition.h
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#ifndef CONDITION_H_
#define CONDITION_H_

#include <pthread.h>

class Mutex;

class Condition {
public:
	Condition();
	~Condition();

	void wait(Mutex &);
	void signal();
	void broadcast();

private:
	pthread_cond_t m_condition;
};

#endif /* CONDITION_H_ */
