# Praktikum 4 main cmake file
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

# GdI 3 Praktikum 4
PROJECT(SceneRenderer)

#------------------------------------------------------------------------------
# options and flags
#------------------------------------------------------------------------------
# Set options
SET(CMAKE_VERBOSE_MAKEFILE off)
SET(CMAKE_BUILD_TYPE "Release")

SET(CMAKE_BINARY_DIR "bin")

# Select cc-flags
SET(CMAKE_CXX_FLAGS "-Wall -Wno-write-strings -pthread")

#SET(CMAKE_CXX_COMPILER "/usr/bin/clang++")
SET(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O3 -g")
SET(CMAKE_CXX_FLAGS_RELEASE "-O3")
SET(CMAKE_CXX_FLAGS_DEBUG  "-O0 -g")

#------------------------------------------------------------------------------
# options and flags
#------------------------------------------------------------------------------
ADD_EXECUTABLE(Praktikum4 aosampler.cpp camera.cpp CBVHBuilder.cpp CScene.cpp ppm_image.cpp vec.cpp pr4.cpp OperationQueue.cpp Mutex.cpp Condition.cpp RaycastingOperation.cpp Thread.cpp)

