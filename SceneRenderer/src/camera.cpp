/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#include <cmath>
#include <ctime>

#include "camera.h"

void CCamera::init(vec3 const & eye, vec3 const & center, vec3 const & up, float fovDegree, unsigned int width, unsigned int height)
{
	srand(time(NULL));

	m_eye = eye;
	// calculate view direction
	m_view = (center - eye).normalize();
	// calculate right vector
	m_right = vec3::cross(m_view, up).normalize();
	// calculate new up vector
	m_up = vec3::cross(m_right, m_view);

	// compute reciprocal dimensions
	m_invWidth = 1.0f / width;
	m_invHeight = 1.0f / height;

	// compute aspect ratio
	float aspectRatio = float(width) * m_invHeight;

	// compute half vertical field of view in radians
	float fovRadHalf = fovDegree / 180.0f * VAL_PI * 0.5f;

	// compute dimensions of the image plane in world space
	// when the plane is one unit away
	m_imagePlaneWorldSpaceHeight = atan(fovRadHalf) * 2.0f;
	m_imagePlaneWorldSpaceWidth = m_imagePlaneWorldSpaceHeight * aspectRatio;
}

Ray CCamera::createRayForPixel(unsigned int xPos, unsigned int yPos, bool jitter)
{

	Ray result;
	// set ray origin to camera position
	result.origin = m_eye;

	float jitteredX = (float)xPos;
	float jitteredY = (float)yPos;

		//jitter effect: randomly distorts effective x/y values by a value between -0.5 to +0.5 each
	//use for multisampling of single pixels
	if(jitter)
	{
		jitteredX += ((float)(rand() % 1000)) / 1000.0 - 0.5;
		jitteredY += ((float)(rand() % 1000)) / 1000.0 - 0.5;
	}

	// convert pixel position to  [-0.5, 0.5] range
	// pixel position (0,0) is mapped to (-0.5, 0.5)
	float normalizedX = jitteredX * m_invWidth - 0.5f;
	float normalizedY = -(jitteredY * m_invHeight - 0.5f);

	// compute direction
	result.direction = m_view + m_right * (normalizedX * m_imagePlaneWorldSpaceWidth) + m_up * (normalizedY * m_imagePlaneWorldSpaceHeight);
	result.direction = result.direction.normalize();

	return result;
}