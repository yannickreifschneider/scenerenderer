/*
 * Thread.cpp
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#include <stddef.h>
#include <iostream>

#include "Thread.h"
#include "OperationQueue.h"
#include "Operation.h"

extern "C"
void *_thread_runner(void *obj)
{
	if (NULL != obj) {
		Thread *t = (Thread *)obj;
		t->main();
	}
	return NULL;
}

Thread::Thread(unsigned int thread_no, OperationQueue *operation_queue)
: m_thread_no(thread_no)
, m_operation_queue(operation_queue)
, m_finished(false)
{
	pthread_create(&m_pthread, NULL, _thread_runner, this);
}

Thread::~Thread()
{
	cancel();
}

void Thread::join()
{
	pthread_join(m_pthread, NULL);
}

void Thread::cancel()
{
	pthread_cancel(m_pthread);
}

void Thread::main()
{
	while (!isFinished()) {
		m_operation_queue->m_queue_mutex.lock();
		while (m_operation_queue->m_operations.empty() && !isFinished()) {
			m_operation_queue->m_empty_queue_mutex.lock();
			m_operation_queue->m_empty_queue_condtion.signal();
			std::cout << "Thread " << m_thread_no << " starved and signaled empty queue" << std::endl;
			m_operation_queue->m_empty_queue_mutex.unlock();
			m_operation_queue->m_queue_condition.wait(m_operation_queue->m_queue_mutex);
		}
		if (!m_operation_queue->m_operations.empty()) {
			Operation *op = m_operation_queue->m_operations.front();
			m_operation_queue->m_operations.pop();
			m_operation_queue->m_queue_mutex.unlock();
			op->run();
		}
		else {
			std::cout << "empty queue in thread " << m_thread_no << " and finished state: " << isFinished() << std::endl;
			m_operation_queue->m_queue_condition.signal();
			m_operation_queue->m_queue_mutex.unlock();
		}
	}
	std::cout << "Thread " << m_thread_no << " finished" << std::endl;

}

