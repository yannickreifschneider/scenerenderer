/*
	This functions are part of the pbrt file parallel.h.
	pbrt source code Copyright(c) 1998-2010 Matt Pharr and Greg Humphreys.
 */

#ifndef ATOMICS_H
#define ATOMICS_H

inline int AtomicCompareAndSwap(volatile int * v, int newValue, int oldValue) {
	int result;
	__asm__ __volatile__("lock\ncmpxchgl %2,%1"
						  : "=a"(result), "=m"(*v)
						  : "q"(newValue), "0"(oldValue)
						  : "memory");
	return result;
}

// performs an atomic add of the value delta to the float value at address val
// the old value at val is returned
inline float AtomicAddFloat(volatile float *val, float delta) {
	union bits { float f; int i; };
	bits oldVal, newVal;
	do {
		// On IA32/x64, adding a PAUSE instruction in compare/exchange loops
		// is recommended to improve performance.  (And it does!)
#if (defined(__i386__) || defined(__amd64__))
		__asm__ __volatile__ ("pause\n");
#endif
		oldVal.f = *val;
		newVal.f = oldVal.f + delta;
	} while (AtomicCompareAndSwap(((volatile int *)val),
								  newVal.i, oldVal.i) != oldVal.i);
	return newVal.f;
}

#endif // ATOMICS_H
