/*
 * Thread.h
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#ifndef THREAD_H_
#define THREAD_H_

#include <pthread.h>
#include <iostream>

class OperationQueue;

class Thread {
public:
	Thread(unsigned int, OperationQueue *);
	~Thread();
	void join();
	void cancel();
	void main();

	bool isFinished() { return m_finished; }
	void setFinished(bool f) { m_finished = f; std::cout << "Thread " << m_thread_no << " set finished state" << std::endl;  }

private:
	bool m_finished;
	unsigned int m_thread_no;
	OperationQueue *m_operation_queue;
	pthread_t m_pthread;
};

#endif /* THREAD_H_ */
