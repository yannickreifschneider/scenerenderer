/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#ifndef CAMERA_H_INCLUDED
#define CAMERA_H_INCLUDED

#include"geometry.h"

class CCamera
{
public:
	/// initialize the camera
	void init(vec3 const & eye, vec3 const & center, vec3 const & up, float fovDegree, unsigned int width, unsigned int height);
	/// creates a ray for the given Pixle position, image origin is top-left, variates these values when jitter is set
	Ray createRayForPixel(unsigned int xPos, unsigned int yPos, bool jitter);
private:
	/// camera position
	vec3 m_eye;
	/// view direction
	vec3 m_view;
	/// right vector
	vec3 m_right;
	/// up vector
	vec3 m_up;

	/// inverse width and height
	float m_invWidth;
	float m_invHeight;

	/// dimensions of the image plane in world space
	/// when the plane is one unit away
	float m_imagePlaneWorldSpaceWidth;
	float m_imagePlaneWorldSpaceHeight;

};

#endif // CAMERA_H_INCLUDED