/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#include "vec.h"

#include <cmath> // needed for sqrt
#include <algorithm>

///////
// vec2
vec2::vec2(float a, float b)
{
	x=a;
	y=b;
}
vec2::vec2()
{
	x=y=0.0;
}

///////
// vec3
vec3::vec3()
{
	x=y=z=0.0;
}

vec3::vec3(float a, float b, float c)
{
	x=a;
	y=b;
	z=c;
}

void vec3::operator=(float v)
{
	x = v;
	y = v;
	z = v;
}

float vec3::length() const
{
	return sqrt(x*x + y*y + z*z);
}

vec3 vec3::normalize() const
{
	vec3 result = *this;
	float invL = 1.0f/length();
	result.x *= invL;
	result.y *= invL;
	result.z *= invL;
	return result;
}

vec3 vec3::cross(const vec3& a, const vec3& b)
{
	vec3 v;

	v.x = (a.y * b.z) - (a.z * b.y);
	v.y = (a.z * b.x) - (a.x * b.z);
	v.z = (a.x * b.y) - (a.y * b.x);

	return v;
}

float vec3::dot(const vec3& a) const
{
	return x*a.x + y*a.y + z*a.z;
}


vec3 vec3::subtract(const vec3& a) const
{
	vec3 result = *this;
	result.x -= a.x;
	result.y -= a.y;
	result.z -= a.z;
	return result;
}

vec3 vec3::add(const vec3& a) const
{
	vec3 result = *this;
	result.x += a.x;
	result.y += a.y;
	result.z += a.z;
	return result;
}

void vec3::operator+=( vec3 const & v )
{
	x += v.x;
	y += v.y;
	z += v.z;
}

void vec3::operator-=( vec3 const & v )
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
}

float vec3::minComponent() const
{
	float temp = std::min(x,y);
	return std::min(temp,z);
}

float vec3::maxComponent() const
{
	float temp = std::max(x,y);
	return std::max(temp,z);
}

vec3 vec3::min(const vec3& a, const vec3& b)
{
	return vec3(std::min(a.x, b.x), std::min(a.y, b.y), std::min(a.z, b.z));
}

vec3 vec3::max(const vec3& a, const vec3& b)
{
	return vec3(std::max(a.x, b.x), std::max(a.y, b.y), std::max(a.z, b.z));
}

const vec3 operator+(const vec3& lhs, const vec3& rhs)
{
	return lhs.add(rhs);
}

const vec3 operator-(const vec3& lhs, const vec3& rhs)
{
	return lhs.subtract(rhs);
}

const vec3 operator-(const vec3& lhs, const float& rhs)
{
	return vec3(lhs.x - rhs, lhs.y - rhs, lhs.z - rhs);
}

const vec3 operator*(const float& lhs, const vec3& rhs)
{
	return vec3(rhs.x*lhs, rhs.y*lhs, rhs.z*lhs);
}

const vec3 operator*(const vec3& lhs, const float& rhs)
{
	return vec3(lhs.x*rhs, lhs.y*rhs, lhs.z*rhs);
}

const vec3 operator*(const vec3& lhs, const vec3& rhs)
{
	return vec3(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z);
}

const vec3 operator/(const vec3& lhs, const float& rhs)
{
	return lhs * (1.0f/rhs);
}

const vec3 operator/(const float& lhs, const vec3& rhs)
{
	return vec3(lhs / rhs.x, lhs / rhs.y, lhs / rhs.z);
}