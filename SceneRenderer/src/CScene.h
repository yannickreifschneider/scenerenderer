/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */


#ifndef SCENE_H_INCLUDED
#define SCENE_H_INCLUDED

#include "geometry.h"
#include <vector>
#include <map>
#include <string>

//////// Forward declarations

struct BVHNode;

/// A Scene represents the combination of a camera and a bunch of objects encapsulated in an accelerationStructure.
/// It also holds a list of materials
class CScene
{
public:

	CScene();

	virtual ~CScene();
public:
	/// load model - invokes loading the materials as well
	bool loadScene(const std::string& filename);

	/// set the file path prefix for the mtlLib
	void setMtlLibPathPrefix(std::string prefix){ m_mtlLibPrefix = prefix; }

	/// find the intersection of a ray with the scene
	bool findIntersection(Ray const & ray, Intersection & intersection) const;

	/// check if a ray intersects the scene
	bool intersects(Ray const & ray) const;

private:

		///A fat vertex index contains indices for position, texture coordinates and normal
	struct FatVertexIndex
	{
		int vertex;
		int texCoords;
		int normal;
	};

	///A model face contains indices into the list of vertices/normals/texCoords, grouped by triangles

	struct ModelFace
	{
		int vertices[3];
		int texCoords[3];
		int normals[3];
	};

	///A group represents a range of faces with the same material

	struct Group
	{
		unsigned int start;
		unsigned int end;
		unsigned int materialID;
	};

	bool loadObjModel(std::string const & filename);

	/// the list of vertices, three vertices in a row make a triangle
	std::vector<PositionNormal> m_vertices;

	/// material id of the vertex
	std::vector<unsigned int> m_materialIds;

	///the material names linked to their index in CScene::m_materials
	std::map<std::string, int> m_materialIdMap;

	///A map of all materials used in the scene, indexed by the name used in the models
	std::vector<vec3> m_materials;

	/// acceleration structure for scene ray intersection
	std::vector<BVHNode> m_bvh;

	///file path prefix for the mtlLib file
	std::string m_mtlLibPrefix;

	///load a material library file
	bool loadMtlLib(const std::string& filename);
};

#endif	// #ifndef SCENE_H_INCLUDED
