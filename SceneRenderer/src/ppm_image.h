/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#ifndef PPM_IMAGE_H_INCLUDED
#define PPM_IMAGE_H_INCLUDED

// saves the ppm image
extern unsigned char savePPM(float const * image, unsigned int width, unsigned int height, char const * fileName);

#endif // #ifndef PPM_IMAGE_H_INCLUDED