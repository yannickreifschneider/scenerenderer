/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

// base include
#include "CScene.h"
#include "geometry.h"
#include "CBVHBuilder.h"

#include <fstream>
#include <iostream>
#include <stack>

//-----------------------------------------------------------------------------
// method        : CScene::CScene()
// params        :
// return values :
// description   : contructor
//-----------------------------------------------------------------------------
CScene::CScene()
:
m_mtlLibPrefix("") {
}

//-----------------------------------------------------------------------------
// method        : CScene::~CScene()
// params        : 
// return values : 	
// description   : destructor
//-----------------------------------------------------------------------------
CScene::~CScene() {}

bool CScene::loadMtlLib(const std::string& filename)
{

	try
	{
		std::ifstream mtl_file(filename.c_str());

		if(mtl_file.fail())
		{
			std::cout << "CScene: wasn't able to load mtl_lib file " << filename << std::endl;
			return false;
		}

		std::string mat_name = "";
		bool haveReadMaterialName = false;

		enum states
		{
			start = 1,
			read_newmtl,
			read_c,
		};
		enum states currState = start;

		while(mtl_file)
		{
			switch(currState)
			{
			case start:
				{
					char c;
					mtl_file >> c;
					switch(c)
					{
					case 'n':
						currState = read_newmtl;
						break;
					case 'c':
						currState = read_c;
						break;
					default:
						std::string temp;
						getline(mtl_file, temp);
						break;
					}
				}
				break;

			case read_c:
				{
					vec3 color;
					mtl_file >> color.x;
					mtl_file >> color.y;
					mtl_file >> color.z;

					if(haveReadMaterialName)
					{
						//push back material
						m_materials.push_back(color);
						unsigned int id = m_materials.size() - 1;
						m_materialIdMap[mat_name] = id;
					} else {
						std::cout << "CScene: specified color before specifying material name" << std::endl;
						return false;
					}

					haveReadMaterialName = false;

					currState = start;
				}
				break;
			case read_newmtl:
				{
					std::string line;
					getline(mtl_file, line);
					std::string::size_type pos = line.find("ewmtl", 0);
					if(pos == std::string::npos)
					{
						std::cout << "CScene: error while parsing material name" << std::endl;
						return false;
					}
					else
					{
						mat_name = line.substr(pos + 6);
						haveReadMaterialName = true;
					}
					currState = start;
				}
				break;
			default:
				currState = start;
				break;
			}
		}

		mtl_file.close();
		return true;
	}
	catch(std::exception& e)
	{
		std::cout << "Exception in CScene::loadMtlLib = " << e.what();
		return false;
	}
}

bool CScene::loadScene(const std::string& filename)
{
	// clear scene data
	m_vertices.clear();
	m_materialIds.clear();
	m_materialIdMap.clear();
	m_materials.clear();

	if(!loadObjModel(filename))
	{
		return false;
	}

	// construct acceleration structure
	m_bvh.clear();
	CBVHBuilder bvhBuilder;
	bvhBuilder.buildBVH(m_bvh, m_vertices, m_materialIds);

	return true;
}


bool CScene::loadObjModel(std::string const & filename)
{
	std::ifstream modelfile(filename.c_str());

	if(modelfile.fail())
	{
		std::cout << "CScene: wasn't able to load file " << filename << std::endl;
		return false;
	}
	else
	{
		std::cout << "CScene: Loading " << filename << std::endl;
	}

	// the list of vertices
	std::vector<vec3> vertices;

	// all texCoords
	std::vector<vec2> texCoords;

	// the list of normals (not necessarily smooth)
	std::vector<vec3> normals;

	// a list of all faces in this model
	std::vector<ModelFace> faces;

	// the groups in this model
	std::vector<Group> groups;

	// add a default material, a nice pink
	m_materials.push_back(vec3(1.0f, 0.0f, 1.0f));
	m_materialIdMap["default"] = 0;

	unsigned int linesRead = 0;

	enum states
	{
		start = 1, have_v, read_vt, read_vn, read_f, read_usemtl, read_mtllib
	};
	enum states currState = start;


	while(modelfile)
	{
		switch(currState)
		{
		case start:
			{
				linesRead++;
				char c;
				std::string temp;
				modelfile >> c;
				switch(c)
				{
				case 'v':
					currState = have_v;
					break; //go to differenciation between v, vt and vn
				case 'f':
					currState = read_f;
					break; //read face
				case 'm':
					currState = read_mtllib;
					break; //read in a materiallib
				case 'u':
					currState = read_usemtl;
					break; //use the specified material for this group
				case '\r':
					break; //skip white space without getline 
				default:
					getline(modelfile, temp);
					//std::cout << temp << std::endl;
					break; //line which doesnt interest ous
				}
			}
			break;

		case have_v:
			{
				char c;
				modelfile.get(c);
				switch(c)
				{
				case 't':
					currState = read_vt;
					break;
				case 'n':
					currState = read_vn;
					break;
				default: //Read Vertex
					vec3 vec;
					modelfile >> vec.x;
					modelfile >> vec.y;
					modelfile >> vec.z;
					vertices.push_back(vec);
					currState = start;
					break;
				}
			}
			break;

		case read_vt: //Read Texture Coordinate
			{
				vec2 vec;
				modelfile >> vec.x;
				modelfile >> vec.y;
				texCoords.push_back(vec);
				currState = start;
			}
			break;

		case read_vn: //Read Vertex Normal
			{
				vec3 vec;
				modelfile >> vec.x;
				modelfile >> vec.y;
				modelfile >> vec.z;
				normals.push_back(vec);
				currState = start;
			}
			break;

		case read_f: //Read triangle
			{
				{
				char c = '/';
				std::vector<FatVertexIndex> faceVertices;

				while(true)
				{
					do
					{
						modelfile.get(c);
					} while(c == ' ' || c == '\t');
					modelfile.unget();

					modelfile.get(c);
					if(c != '\n')
					{
						modelfile.unget();
						FatVertexIndex fvi;
						fvi.vertex = -1;
						fvi.texCoords = -1;
						fvi.normal = -1;

						modelfile >> fvi.vertex;
						modelfile.get(c);
						if(c == '/')
						{
							modelfile >> fvi.texCoords;
							modelfile.get(c);
							if(c == '/')
							{
								modelfile >> fvi.normal;
							}
						}
						faceVertices.push_back(fvi);
					} else 
						break;
				}

				if(faceVertices.size() < 3)
				{
					std::cout << "CScene: Found face with " << faceVertices.size() << " vertices" << " in line " << linesRead << std::endl;
					return false;
				}

				for(unsigned int i = 0; i < faceVertices.size() - 2; ++i)
				{
					ModelFace f;
					f.vertices[0] = faceVertices[i].vertex - 1;
					f.texCoords[0] = faceVertices[i].texCoords - 1;
					f.normals[0] = faceVertices[i].normal - 1;

					f.vertices[1] = faceVertices[i+1].vertex - 1;
					f.texCoords[1] = faceVertices[i+1].texCoords - 1;
					f.normals[1] = faceVertices[i+1].normal - 1;

					f.vertices[2] = faceVertices[i+2].vertex - 1;
					f.texCoords[2] = faceVertices[i+2].texCoords - 1;
					f.normals[2] = faceVertices[i+2].normal - 1;
								
					vec3 p0 = vertices[f.vertices[0]];
					vec3 p1 = vertices[f.vertices[1]];
					vec3 p2 = vertices[f.vertices[2]];
					vec3 min = p0;
					vec3 max = p0;
					min.x = p1.x < min.x ? p1.x : min.x;
					min.y = p1.y < min.y ? p1.y : min.y;
					min.z = p1.z < min.z ? p1.z : min.z;
					max.x = p1.x > max.x ? p1.x : max.x;
					max.y = p1.y > max.y ? p1.y : max.y;
					max.z = p1.z > max.z ? p1.z : max.z;

					min.x = p2.x < min.x ? p2.x : min.x;
					min.y = p2.y < min.y ? p2.y : min.y;
					min.z = p2.z < min.z ? p2.z : min.z;
					max.x = p2.x > max.x ? p2.x : max.x;
					max.y = p2.y > max.y ? p2.y : max.y;
					max.z = p2.z > max.z ? p2.z : max.z;

					float ba = max.x - min.x;
					float bb = max.y - min.y;
					float bc = max.z - min.z;
					float sa = 2.0f*(ba*bb+ba*bc+bb*bc);

					//if(sa > 0.0001f)
						faces.push_back(f);
				}
				}
				currState = start;
			}
			break;

		case read_mtllib:
			{
				std::string line;
				getline(modelfile, line);
				std::string::size_type pos = line.find("tllib", 0);
				if(pos == std::string::npos)
				{
					std::cout << "CScene: Error while parsing mtllib keyword" << " in line " << linesRead << std::endl;
					return false;
				}
				else
				{
					std::string mtl_lib = line.substr(pos + 6);

					if(!loadMtlLib(m_mtlLibPrefix+mtl_lib))
						return false;
				}
				currState = start;
			}
			;
			break;
		case read_usemtl:
			{
				std::string line;
				getline(modelfile, line);
				std::string::size_type pos = line.find("semtl", 0);
				if(pos == std::string::npos)
				{
					std::cout << "CScene: Error while parsing usemtl keyword" << " in line " << linesRead << std::endl;
					return false;
				}
				else
				{
					if(!groups.empty())
					{
						groups.back().end = faces.size();
					}
					Group g;
					g.start = faces.size();
					std::string usemtl = line.substr(pos + 6);

					if(m_materialIdMap.find(usemtl) != m_materialIdMap.end())
					{
						g.materialID = m_materialIdMap[usemtl];
					}
					else
					{
						std::cout << "CScene: could not find secified material: " << usemtl << " in line " << linesRead << std::endl;
						g.materialID = 0;
					}

					groups.push_back(g);
					currState = start;
				}
			}
			;
			break;
		default:
			currState = start;
			break;
		}
	}

	if(!groups.empty())
	{
		groups.back().end = faces.size();
	}
	else
	{
		Group g;
		g.start = 0;
		g.end = faces.size();
		g.materialID = 0; // default material
		groups.push_back(g);

		std::cout << "CScene: no groups specified. Generated default group with default material." << std::endl;
	}


	// transform to triangle list
	m_vertices.reserve( faces.size() * 3 );
	m_materialIds.reserve( faces.size() );

	for(unsigned int i = 0; i < groups.size(); ++i)
	{
		Group const & g = groups[i];
		for(unsigned int j = g.start; j < g.end; ++j)
		{
			PositionNormal p0;
			PositionNormal p1;
			PositionNormal p2;
			// fetch positions
			p0.position = vertices[faces[j].vertices[0]];
			p1.position = vertices[faces[j].vertices[1]];
			p2.position = vertices[faces[j].vertices[2]];
			// set normals
			if( faces[j].normals[0] >= 0 && faces[j].normals[1] >= 0 && faces[j].normals[2] >= 0)
			{// fetch normals
				p0.normal = normals[faces[j].normals[0]];
				p1.normal = normals[faces[j].normals[1]];
				p2.normal = normals[faces[j].normals[2]];
			} else {
				// set to face normal
				vec3 d1 = p1.position - p0.position;
				vec3 d2 = p2.position - p0.position;
				vec3 faceNormal = vec3::cross(d1, d2).normalize();
				p0.normal = faceNormal;
				p1.normal = faceNormal;
				p2.normal = faceNormal;
			}
			m_vertices.push_back(p0);
			m_vertices.push_back(p1);
			m_vertices.push_back(p2);

			// set triangle material to group material
			m_materialIds.push_back(g.materialID);
		}
	}


	std::cout << "CScene: finished loading obj file, " << faces.size() << " faces, " << groups.size() << " groups." << std::endl;
	std::cout << "from " << m_vertices.size() << " vertices. (" << linesRead << " lines)" << std::endl;

	return true;
}

bool CScene::findIntersection(Ray const & ray, Intersection & intersection) const
{
	vec3 invRayDir = 1.0f / ray.direction;

	// does the ray hit the root?
	if(!intersectsRayBB(ray.origin, invRayDir, m_bvh[0].bb, ray.tMin, ray.tMax))
		return false;
	
	std::stack<unsigned int> traversalStack;

	unsigned int const INVALID_TRIANGLE_IDX = 0xffffffffu;

	float isectT = ray.tMax;
	unsigned int isectTriangle = INVALID_TRIANGLE_IDX;

	traversalStack.push(0u); // push root

	while(!traversalStack.empty())
	{
		unsigned int nodeIdx = traversalStack.top();
		traversalStack.pop();
		// found leaf or nothing
		BVHNode const & currentNode = m_bvh[nodeIdx];
		if(!currentNode.isLeaf())
		{// inner node
			unsigned int childIdx = currentNode.val1;

			BVHNode const & child1 = m_bvh[childIdx];
			BVHNode const & child2 = m_bvh[childIdx+1];

			float minT1 = ray.tMin;
			float maxT1 = isectT;
			float minT2 = ray.tMin;
			float maxT2 = isectT;

			bool intersectedChild1 = intersectRayBB(ray.origin,invRayDir,child1.bb, minT1, maxT1);
			bool intersectedChild2 = intersectRayBB(ray.origin,invRayDir,child2.bb, minT2, maxT2);

			if(!intersectedChild1 && !intersectedChild2)
				continue;

			unsigned int nearChild = intersectedChild1 ? childIdx : (childIdx+1);

			// if both children intersected, we have to push the farther one first on the stack
			if(intersectedChild1 && intersectedChild2)
			{
				unsigned int farChild = childIdx+1;
				if(minT2 < minT1)
				{
					nearChild = childIdx+1;
					farChild = childIdx;
				}
				traversalStack.push(farChild);
			}
			traversalStack.push(nearChild);
		} else { // leaf node
			unsigned int triangleStart = currentNode.val1;
			unsigned int triangleCount = currentNode.val2;
			for(unsigned int triangleIndex = 0; triangleIndex < triangleCount; ++triangleIndex)
			{
				float t;
				unsigned int vertexIndex = (triangleStart + triangleIndex) * 3;
				vec3 p = m_vertices[vertexIndex].position;
				vec3 edge1 = m_vertices[vertexIndex+1].position;
				vec3 edge2 = m_vertices[vertexIndex+2].position;
				t = intersectRayTriangleT(ray, p, edge1, edge2);

				if(t > 0.0001f && (t < isectT))
				{
					isectT = t;
					isectTriangle = triangleStart + triangleIndex;
				}
			}
		}
	} // END: tree traversal

	if(isectTriangle == INVALID_TRIANGLE_IDX)
		return false;
	// found a triangle, collect hitpoint informations
	unsigned int vertexIndex = isectTriangle * 3;
	// fetch intersection triangle vertices
	PositionNormal const & p = m_vertices[vertexIndex];
	PositionNormal const & edge1 = m_vertices[vertexIndex+1];
	PositionNormal const & edge2 = m_vertices[vertexIndex+2];

	// calculate barycentric coordinates of the hitpoint
	float u,v;
	intersectRayTriangleUV(ray, p.position, edge1.position, edge2.position, u, v);

	// compute hitpoint position from barycentric coordinates
	intersection.position = p.position + u * edge1.position + v * edge2.position;

	// get triangle material
	intersection.color = m_materials[m_materialIds[isectTriangle]];

	// compute interpolated hitpoint normal from barycentric coordinates
	vec3 interpolatedNormal = (1.0f - u - v) * p.normal;
	interpolatedNormal += u * edge1.normal;
	interpolatedNormal += v * edge2.normal;
	intersection.normal = interpolatedNormal.normalize();

	return true;
}

bool CScene::intersects(Ray const & ray) const
{
	vec3 invRayDir = 1.0f / ray.direction;

	// does the ray hit the root?
	if(!intersectsRayBB(ray.origin, invRayDir, m_bvh[0].bb, ray.tMin, ray.tMax))
		return false;
	
	std::stack<unsigned int> traversalStack;

	float isectT = ray.tMax;

	traversalStack.push(0u);

	while(!traversalStack.empty())
	{
		unsigned int nodeIdx = traversalStack.top();
		traversalStack.pop();
		// found leaf or nothing
		BVHNode const & currentNode = m_bvh[nodeIdx];
		if(!currentNode.isLeaf())
		{// inner node
			unsigned int childIdx = currentNode.val1;

			BVHNode const & child1 = m_bvh[childIdx];
			BVHNode const & child2 = m_bvh[childIdx+1];

			float minT1 = ray.tMin;
			float maxT1 = isectT;
			float minT2 = ray.tMin;
			float maxT2 = isectT;

			bool intersectedChild1 = intersectRayBB(ray.origin,invRayDir,child1.bb, minT1, maxT1);
			bool intersectedChild2 = intersectRayBB(ray.origin,invRayDir,child2.bb, minT2, maxT2);

			if(!intersectedChild1 && !intersectedChild2)
				continue;

			unsigned int nearChild = intersectedChild1 ? childIdx : (childIdx+1);

			// if both children intersected, we have to push the farther one first on the stack
			if(intersectedChild1 && intersectedChild2)
			{
				unsigned int farChild = childIdx+1;
				if(minT2 < minT1)
				{
					nearChild = childIdx+1;
					farChild = childIdx;
				}
				traversalStack.push(farChild);
			}
			traversalStack.push(nearChild);
		} else { // leaf node
			unsigned int triangleStart = currentNode.val1;
			unsigned int triangleCount = currentNode.val2;
			for(unsigned int triangleIndex = 0; triangleIndex < triangleCount; ++triangleIndex)
			{
				float t;
				unsigned int vertexIndex = (triangleStart + triangleIndex) * 3;
				vec3 p = m_vertices[vertexIndex].position;
				vec3 edge1 = m_vertices[vertexIndex+1].position;
				vec3 edge2 = m_vertices[vertexIndex+2].position;
				t = intersectRayTriangleT(ray, p, edge1, edge2);

				if(t > 0.0001f && (t < isectT))
				{
					return true;
				}
			}
		}
	} // END: tree traversal
	return false;
}