/**
 * @author	Bruno Schneewittchen
 * @date 29.11.2011
 */

#include "ppm_image.h"

#include <stdio.h>			// include for fprintf
#include <cmath>

unsigned char savePPM(float const * image, unsigned int width, unsigned int height, char const * fileName)
{
	// create a new text file
	FILE* file = fopen(fileName, "w");

	if(!file) {
		fprintf(stderr, "Could not create ppm file.\n");
		return 0;
	}

	//-------------------------------------------------------------------------
	// write image data
	//-------------------------------------------------------------------------
	// write magic
	fprintf(file,"P3\n");

	// write width and height
	fprintf(file,"%d %d\n", width, height);

	// write the max gray value
	fprintf(file,"%d\n", 255);

	unsigned int numPixels = width * height;
	for(unsigned int i = 0; i < numPixels; ++i) {
		unsigned int r = (unsigned int)(pow(image[0],1.0f/2.2f) * 255);
		unsigned int g = (unsigned int)(pow(image[1],1.0f/2.2f) * 255);
		unsigned int b = (unsigned int)(pow(image[2],1.0f/2.2f) * 255);
		fprintf(file,"%d %d %d\n", r, g, b);
		// advance by a pixel (3 floats)
		image += 3;
	}

	//
	fclose(file);
	
	return 1;
}