#include <iostream>

#include "CScene.h"
#include "camera.h"
#include "aosampler.h"
#include "geometry.h"
#include "ppm_image.h"
#include "atomics.h"
#include "OperationQueue.h"
#include "RaycastingOperation.h"

void renderSequentially()
{
	CScene * scene = new CScene();

	// load scene
	if(!scene->loadScene("sponza.obj"))
	{
		std::cout << "Epic failure!" << std::endl;
		delete scene;
		return;
	}

	// some constants
	unsigned int const imageWidth = 768;
	unsigned int const imageHeight = 768;
	float const verticalFieldOfView = 60.0f;

	unsigned int numSamplesPerPixel = 4;

	unsigned int aoNumSamples = 16; // have to quadruple samples to half the noise
	float const aoCheckDistance = 800.0f;

	// allocate memory for picture
	float * imageData = new float[imageWidth * imageHeight * 3];
	float * imageIterator = imageData;

	// setup camera
	CCamera camera;
	camera.init( vec3(1000.0f,200.2f,100.9f), // camera position
	             vec3(0.0f,500.0f,0.0f), // lookAt
				 vec3(0.0f,1.0f,0.0f), // upVector
				 verticalFieldOfView, imageWidth, imageHeight);

	std::cout << "Rendering..." << std::endl;

	// iterate over pixels and compute colors
	for(int y = 0; y < imageHeight; ++y)
	{
		float percentage = float(y) / (imageHeight-1) * 100.0f;
		std::cout << "Progress: " << percentage << '%' << std::endl;
		for(int x = 0; x < imageWidth; ++x, imageIterator += 3)
		{
			imageIterator[0] = 0.0f;
			imageIterator[1] = 0.0f;
			imageIterator[2] = 0.0f;
			for(unsigned int ps = 0; ps < numSamplesPerPixel; ++ps)
			{
				// get camera ray for the pixel
				Ray ray = camera.createRayForPixel(x,y, true);
				Intersection intersection;

				vec3 color;
				// check for scene intersection
				if( scene->findIntersection(ray, intersection) )
				{
					// found intersection, apply AO shading
					color = vec3(0.0f,0.0f,0.0f);
					// initialize sampler
					AOSampler sampler(intersection.normal, intersection.color);
					// init test ray
					Ray testRay;
					testRay.origin = intersection.position;
					testRay.tMax = aoCheckDistance;
					// generate samples
					for(unsigned int s = 0; s < aoNumSamples; ++s)
					{
						vec3 contribution;
						sampler.sample(testRay.direction, contribution);
						if(!scene->intersects(testRay))
							color += contribution; // add conribution if unoccluded
					}
					color = color / float(aoNumSamples);
				} else {
					// set to a default color
					color = vec3(0.9f,0.9f,1.0f);
				}
				color = color / float(numSamplesPerPixel);
				// store value in image
				imageIterator[0] += color.x;
				imageIterator[1] += color.y;
				imageIterator[2] += color.z;
			}
		}
	}
	std::cout << "Finished rendering" << std::endl;

	// write image to file
	savePPM(imageData, imageWidth, imageHeight, "result.ppm");

	delete[] imageData;
	delete scene;
}

void renderConcurrently(int color_samples, int ao_samples, int num_threads)
{
	CScene * scene = new CScene();

	// load scene
	if(!scene->loadScene("sponza.obj"))
	{
		std::cout << "Epic failure!" << std::endl;
		delete scene;
		return;
	}

	// some constants
	unsigned int const imageWidth = 768;
	unsigned int const imageHeight = 768;
	float const verticalFieldOfView = 60.0f;

	//unsigned int numSamplesPerPixel = 4;

	//unsigned int aoNumSamples = 16; // have to quadruple samples to half the noise
	float const aoCheckDistance = 800.0f;

	// allocate memory for picture
	float *imageData = (float *)calloc(imageWidth * imageHeight * 3, sizeof(float));
	float *imageIterator = imageData;

	// setup camera
	CCamera camera;
	camera.init( vec3(1000.0f,200.2f,100.9f), // camera position
	             vec3(0.0f,500.0f,0.0f), // lookAt
				 vec3(0.0f,1.0f,0.0f), // upVector
				 verticalFieldOfView, imageWidth, imageHeight);

	OperationQueue *queue = new OperationQueue(num_threads);

	std::cout << "Rendering..." << std::endl;

	// iterate over pixels and compute colors
	for(int y = 0; y < imageHeight; ++y)
	{
		float percentage = float(y) / (imageHeight-1) * 100.0f;
		//std::cout << "Progress: " << percentage << '%' << std::endl;
		for(int x = 0; x < imageWidth; ++x, imageIterator += 3)
		{
			RaycastingOperationArgument *arg = (RaycastingOperationArgument *)malloc(sizeof(RaycastingOperationArgument));
			arg->camera = &camera;
			arg->scene = scene;
			arg->data = imageIterator;
			arg->num_color_samples = color_samples;
			arg->num_ao_samples = ao_samples;
			arg->x = x;
			arg->y = y;

			RaycastingOperation *op = new RaycastingOperation((void *)arg);
			queue->addOperation(op);
		}
	}

	std::cout << "waiting for threads to finish..." << std::endl;
	queue->waitUntilAllOperationsAreFinished();
	std::cout << "Finished rendering" << std::endl;

	// write image to file
	savePPM(imageData, imageWidth, imageHeight, "result_conc.ppm");

	free(imageData);
	delete scene;
}

int main(int argc, char * argv[])
{
	/*
	 * for your version, replace the code below, as well as the include section, with your own code.
         * feel free to get inspiration from the sequential renderer as defined in renderSequentially()
	 */
	//renderSequentially();
	if (argc < 3) {
		std::cout << argv[0] << " <num_color_samples> <num_ao_samples> <num_threads>" << std::endl;
		return 1;
	}

	int color_samples = atoi(argv[1]);
	int ao_samples = atoi(argv[2]);
	int threads = atoi(argv[3]);

	renderConcurrently(color_samples, ao_samples, threads);

	return 0;
}
