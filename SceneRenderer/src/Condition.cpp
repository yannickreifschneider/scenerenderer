/*
 * Condition.cpp
 *
 *  Created on: 14.01.2012
 *      Author: yannick
 */

#include <stddef.h>

#include "Condition.h"
#include "Mutex.h"

Condition::Condition()
{
	pthread_cond_init(&m_condition, NULL);
}

Condition::~Condition()
{
	pthread_cond_destroy(&m_condition);
}

void Condition::wait(Mutex &m)
{
	pthread_cond_wait(&m_condition, &m.m_mutex);
}

void Condition::signal()
{
	pthread_cond_signal(&m_condition);
}

void Condition::broadcast()
{
	pthread_cond_broadcast(&m_condition);
}
